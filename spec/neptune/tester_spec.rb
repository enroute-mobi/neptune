# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Neptune::Tester do
  describe '.tester_for' do
    subject { Neptune::Tester.tester_for(filename) }

    context "when filename is 'test.zip'" do
      let(:filename) { 'test.zip' }
      it { is_expected.to eq(Neptune::Tester::Zip) }
    end

    context "when filename is 'test.ZIP'" do
      let(:filename) { 'test.ZIP' }
      it { is_expected.to eq(Neptune::Tester::Zip) }
    end

    context "when filename is 'test.xml'" do
      let(:filename) { 'test.xml' }
      it { is_expected.to eq(Neptune::Tester::XML) }
    end

    context "when filename is 'test.XML'" do
      let(:filename) { 'test.XML' }
      it { is_expected.to eq(Neptune::Tester::XML) }
    end
  end
end

RSpec.describe Neptune::Tester::XML do
  subject(:tester) { Neptune::Tester::XML.new(input) }

  describe '#neptune_file?' do
    subject { tester.neptune_file? }

    [
      %(<?xml version="1.0" encoding="utf-8"?>\n<ChouettePTNetwork>),
      %(<?xml version="1.0" encoding="utf-8"?>\n<ChouettePTNetwork xmlns="http://www.trident.org/schema/trident">)
    ].each do |content|
      context "when the file starts with '#{content}'" do
        let(:input) { StringIO.new(content) }
        it { is_expected.to be_truthy }
      end
    end

    context 'when the file starts with anything else' do
      let(:input) { StringIO.new(%(<?xml version="1.0" encoding="utf-8"?>\n<AnythingElse )) }
      it { is_expected.to be_falsy }
    end
  end
end
