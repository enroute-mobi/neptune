# frozen_string_literal: true

require 'spec_helper'

describe Neptune::Source do
  alias_method :source, :subject

  describe '.accept?' do
    subject { Neptune::Source.accept?(file) }

    context 'with a Neptune XML file' do
      let(:file) { File.expand_path('../fixtures/sample.xml', __dir__) }
      it { is_expected.to be_truthy }
    end

    context 'with a Neptune ZIP file' do
      let(:file) { File.expand_path('../fixtures/sample.zip', __dir__) }
      it { is_expected.to be_truthy }
    end

    context 'with a dummy text file' do
      let(:file) { __FILE__ }
      it { is_expected.to be_falsy }
    end

    context 'with a dummy ZIP file' do
      let(:file) { File.expand_path('../fixtures/dummy.zip', __dir__) }
      it { is_expected.to be_falsy }
    end

    context 'with a empty ZIP file' do
      let(:file) { File.expand_path('../fixtures/empty.zip', __dir__) }
      it { is_expected.to be_falsy }
    end
  end

  describe 'Timetable' do
    describe 'when defined in several XML parts' do
      let(:first_timetable) do
        <<-XML
        <Timetable>
          <objectId>42</objectId>
          <vehicleJourneyId>first</vehicleJourneyId>
        </Timetable>
        XML
      end

      let(:second_timetable) do
        <<-XML
        <Timetable>
          <objectId>42</objectId>
          <vehicleJourneyId>second</vehicleJourneyId>
        </Timetable>
        XML
      end

      let(:source) do
        Neptune::Source.new.tap do |source|
          [first_timetable, second_timetable].each do |xml_definition|
            source.parse StringIO.new xml_definition
          end
          source.flush
        end
      end

      subject(:merged_timetable) do
        source.timetables.first
      end

      it 'unique' do
        expect(source.timetables).to be_one
      end

      it 'contains all the vehicle journey ids' do
        expect(merged_timetable.vehicle_journey_ids).to match_array(%w[first second])
      end
    end
  end

  context 'with a XML sample file' do
    let(:input) { File.new(File.expand_path('../fixtures/sample.xml', __dir__)) }

    before do
      source.parse(input)
      source.flush
    end

    it 'reads 382 StopPoints' do
      expect(source.stop_points.count).to eq(382)
    end

    describe 'First StopPoint' do
      let(:stop_point) { source.stop_points.find { |s| s.id == 'UT21:StopPoint:3602' } }

      expected_attributes = {
        name: 'Gare Dijon ville',
        version: '1'
      }

      expected_attributes.each do |attribute_name, value|
        it "#{attribute_name} is #{value.inspect}" do
          expect(stop_point.send(attribute_name)).to eq(value)
        end
      end
    end

    describe 'Company' do
      let(:company) { source.companies.first }

      expected_attributes = {
        id: 'UT21:Company:1',
        name: 'EXPLOITANT INDETERMINE'
      }

      expected_attributes.each do |attribute_name, value|
        it "#{attribute_name} is #{value.inspect}" do
          expect(company.send(attribute_name)).to eq(value)
        end
      end
    end

    describe 'Line' do
      let(:line) { source.lines.first }

      expected_attributes = {
        id: 'UT21:Line:117',
        name: 'DIJON BLIGNY SUR OUCHE',
        transport_mode_name: 'Coach'
      }

      expected_attributes.each do |attribute_name, value|
        it "#{attribute_name} is #{value.inspect}" do
          expect(line.send(attribute_name)).to eq(value)
        end
      end
    end

    describe 'VehicleJourney' do
      let(:vehicle_journey) { source.vehicle_journeys.first }

      expected_attributes = {
        id: 'UT21:VehicleJourney:117x117-A1x20180627x1x1',
        route_id: 'UT21:Route:117xA',
        journey_pattern_id: 'UT21:JourneyPattern:117x117-A1x20180627x1'
      }

      expected_attributes.each do |attribute_name, value|
        it "#{attribute_name} is #{value.inspect}" do
          expect(vehicle_journey.send(attribute_name)).to eq(value)
        end
      end

      it 'has 17 Vehicle Journey at Stops' do
        expect(vehicle_journey.vehicle_journey_at_stops.size).to eq(17)
      end

      describe 'VehicleJourneyAtStops' do
        let(:vehicle_journey_at_stop) { vehicle_journey.stops.first }

        expected_attributes = {
          stop_point_id: 'UT21:StopPoint:3602',
          arrival_time: '07:20:00',
          departure_time: '07:20:00'
        }

        expected_attributes.each do |attribute_name, value|
          it "#{attribute_name} is #{value.inspect}" do
            expect(vehicle_journey_at_stop.send(attribute_name)).to eq(value)
          end
        end
      end
    end

    describe 'Timetable' do
      let(:timetable) { source.timetables.first }

      expected_attributes = {
        id: 'UT21:TimeTable:71',
        day_types: %w[Monday Tuesday Thursday],
        calendar_days: %w[2020-06-08 2020-06-15 2020-06-22].map { |d| Date.parse d },
        vehicle_journey_ids: %w[UT21:VehicleJourney:117x117-A1x20180627x1x1
                                UT21:VehicleJourney:117x117-R1x20180627x9x1].to_set,
        periods: [
          Neptune::Period.new(begin: '2020-01-06', end: '2020-01-11'),
          Neptune::Period.new(begin: '2020-01-13', end: '2020-01-18'),
          Neptune::Period.new(begin: '2020-01-20', end: '2020-01-25'),
          Neptune::Period.new(begin: '2020-01-27', end: '2020-02-01')
        ]
      }

      expected_attributes.each do |attribute_name, value|
        it "#{attribute_name} is #{value.inspect}" do
          expect(timetable.send(attribute_name)).to eq(value)
        end
      end
    end
  end

  context 'with a ZIP sample file' do
    let(:input) { File.new(File.expand_path('../fixtures/sample.zip', __dir__)) }

    before { source.read(input) }

    it 'reads 340 StopPoints' do
      expect(source.stop_points.count).to eq(340)
    end

    describe 'VehicleJourneys' do
      it 'are associated to a Line (via line_id)' do
        expect(source.vehicle_journeys).to all(have_attributes(line_id: a_string_matching(/UT25:Line:/)))
      end
    end

    describe 'Lines' do
      it 'are associated to a Company (via company_id)' do
        expect(source.lines).to all(have_attributes(company_id: a_string_matching(/UT25:Company:/)))
      end
    end
  end

  describe 'non UTF-8 file support' do
    let(:xml) do
      '<?xml version="1.0" encoding="iso-8859-1"?><Company><name>Norrlandsvägen</name></Company>'.encode('iso-8859-1')
    end

    let(:source) do
      Neptune::Source.new.tap do |source|
        source.parse StringIO.new xml
        source.flush
      end
    end

    let(:resource) { source.companies.first }

    it 'should read resources with UTF-8' do
      expect(resource.name.encoding.name).to eq('UTF-8')
      expect(resource.name).to eq('Norrlandsvägen')
    end
  end
end
