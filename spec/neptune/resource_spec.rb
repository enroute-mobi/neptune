# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Neptune::Period do
  describe '#==' do
    let(:period) { Neptune::Period.new(begin: '2020-01-06', end: '2020-01-11') }

    subject { period == other }

    context 'with the same instance of period' do
      let(:other) { period }
      it { is_expected.to be_truthy }
    end

    context 'with an instance with the same begin and end values' do
      let(:other) { Neptune::Period.new(begin: period.begin, end: period.end) }
      it { is_expected.to be_truthy }
    end
  end
end
