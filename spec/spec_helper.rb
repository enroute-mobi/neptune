# frozen_string_literal: true

unless ENV['NO_RCOV']
  require 'simplecov'

  SimpleCov.start do
    add_filter 'vendor'
    add_filter 'spec'

    if ENV['CODACY_PROJECT_TOKEN']
      require 'simplecov-cobertura'
      formatter SimpleCov::Formatter::CoberturaFormatter
    end

    enable_coverage :branch
  end
end

require 'neptune'
