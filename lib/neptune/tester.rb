# frozen_string_literal: true

module Neptune
  module Tester
    def self.neptune_file?(filename)
      tester_for(filename).open(filename, &:neptune_file?)
    end

    def self.tester_for(filename)
      File.extname(filename).downcase == '.zip' ? Zip : XML
    end

    class Zip
      def initialize(file)
        @file = file
      end
      attr_reader :file

      def self.open(filename)
        yield new(filename)
      end

      def each_xml_entry(&block)
        ::Zip::File.open(file) do |zip_file|
          zip_file.glob('**/*.xml').each do |entry|
            block.call XML.new(entry.get_input_stream)
          end
        end
      end

      def neptune_file?
        empty = true

        each_xml_entry do |xml_entry|
          empty = false
          return false unless xml_entry.neptune_file?
        end

        !empty
      end
    end

    class XML
      def initialize(io)
        @io = io
      end
      attr_reader :io
      private :io

      def self.open(filename)
        instance = new File.new(filename)
        begin
          yield instance
        ensure
          instance.close
        end
      end

      def first_lines
        @first_lines ||= 5.times.map { io.gets }.compact
      end

      # Should like this:
      #
      # <?xml version="1.0" encoding="utf-8"?>
      # <ChouettePTNetwork xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      #  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
      #  xmlns="http://www.trident.org/schema/trident">
      #   <PTNetwork>
      def neptune_file?
        first_lines.any? do |line|
          /<[^ ]*ChouettePTNetwork/ =~ line
        end
      end

      def close
        io.close
      end
    end
  end
end
