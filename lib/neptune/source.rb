# frozen_string_literal: true

module Neptune
  class Source
    def self.accept?(file)
      Tester.neptune_file?(file)
    end

    attr_accessor :offline_threshold, :include_raw_xml, :supported_resources

    def initialize(options = {})
      @collections = Hash.new { |h, k| h[k] = MemoryCollection.new }
      @offline_threshold = 5000
      @include_raw_xml = false
      @supported_resources = self.class.supported_resources

      self.class.default_transformer_classes.each do |transformer_class|
        transformers << transformer_class.new
      end

      options.each { |k, v| send "#{k}=", v }
    end

    class ContextTransformer
      def call(resource, context = {})
        context.each do |attribute, value|
          setter_method = "#{attribute}="
          resource.send setter_method, value if resource.respond_to?(setter_method)
        end

        resource
      end
    end

    class TimetableMerger
      def timetables
        @timetables ||= {}
      end

      def pending_resources
        timetables.values
      end

      def merge(timetable)
        if (existing_timetable = timetables[timetable.id])
          existing_timetable.vehicle_journey_ids.merge timetable.vehicle_journey_ids
        else
          timetables[timetable.id] = timetable
        end

        nil
      end

      def call(resource, _context = {})
        if resource.is_a?(Neptune::Timetable)
          merge resource
        else
          resource
        end
      end
    end

    class UniquenessTransformer
      def known_identifiers
        @known_identifiers ||= Hash.new { |h, k| h[k] = Set.new }
      end

      def call(resource, _context = {})
        return unless known_identifiers[resource.name_of_class].add? resource.id

        resource
      end
    end

    DEFAULT_TRANSFORMER_CLASSES = [TimetableMerger, UniquenessTransformer, ContextTransformer].freeze
    def self.default_transformer_classes
      DEFAULT_TRANSFORMER_CLASSES
    end

    SUPPORTED_RESOURCES = %i[StopPoint Company Line VehicleJourney Timetable].freeze

    def self.supported_resources
      SUPPORTED_RESOURCES
    end

    def add(resource, context = {})
      transformers.each do |transformer|
        resource = transformer.call resource, context
        return if resource.nil?
      end

      add_without_transformers resource
    end

    def add_without_transformers(resource)
      collection = collection_for resource.class
      collection << resource

      offline_collection!(resource.class) if offline_required?(collection)

      resource
    end

    def transformers
      @transformers ||= []
    end

    def flush
      transformers.each do |transformer|
        next unless transformer.respond_to?(:pending_resources)

        transformer.pending_resources.each do |resource|
          add_without_transformers resource
        end
      end
    end

    def close
      @collections.each_value(&:close)
    end

    def collection_for(resource_class)
      @collections[resource_class.name_of_class]
    end

    def offline_collection!(resource_class)
      collection = collection_for(resource_class)
      @collections[resource_class.name_of_class] = OfflineCollection.new(collection)
    end

    def offline_required?(collection)
      collection.memory? &&
        offline_threshold >= 0 && collection.count > offline_threshold
    end

    def resource_count
      @collections.values.sum(&:count)
    end

    # Define stop_places, lines, etc methods to access collections
    supported_resources.each do |resource_class_name|
      # FIXME: for xml-source
      resource_class = Object.const_get "Neptune::#{resource_class_name}"
      pluralized_name =
        if resource_class_name =~ /ny$/
          "#{resource_class.name_of_class[0..-2]}ies"
        else
          "#{resource_class.name_of_class}s"
        end

      define_method(pluralized_name) { collection_for resource_class }
    end

    class Collection
      include Enumerable
    end

    class MemoryCollection < Collection
      def initialize
        @resources = []
      end

      def add(resource)
        @resources << resource
      end
      alias << add

      def each(&block)
        @resources.each(&block)
      end

      def count
        @resources.count
      end

      def close
        @resources.clear
      end

      def memory?
        true
      end
    end

    class OfflineCollection < Collection
      def initialize(existing_collection = nil)
        @file = Tempfile.new
        @resources_count = 0

        return unless existing_collection

        existing_collection.each do |resource|
          add resource
        end
      end

      def add(resource)
        @resources_count += 1
        @file.write(Marshal.dump(resource))
      end
      alias << add

      def each
        @file.rewind

        begin
          loop do
            yield Marshal.load(@file)
          end
        rescue EOFError
          # End Of the File, End Of the Loop
        end

        self
      end

      def count
        @resources_count
      end

      def close
        @file.close
        @file.unlink
      end

      def memory?
        false
      end
    end

    class SaxeratorParser
      def initialize(supported_resources, &block)
        @supported_resources = supported_resources
        @callback = block
      end

      attr_reader :supported_resources

      def parse(input)
        parser = Saxerator.parser(input) do |config|
          config.adapter = :ox
          config.output_type = :xml
        end

        parser.for_tags(supported_resources).each do |xml_values|
          @callback.call xml_values
        end
      end
    end

    def document_formater
      @document_formater ||= REXML::Formatters::Pretty.new(2).tap do |formatter|
        formatter.compact = true
      end
    end

    def parser(context = {})
      return @parser if @parser && context.empty?

      parser = SaxeratorParser.new(supported_resources) do |document|
        resource = Mapper.for(document.root).map
        next unless resource

        if include_raw_xml
          # Remove unwanted XMLDecl added by Saxerator XmlBuilder
          document.instance_eval do
            @children.shift if @children[0].is_a? ::REXML::XMLDecl
          end

          resource.raw_xml = ''
          document_formater.write document, resource.raw_xml
          # document.write(indent: 2, output: resource.raw_xml)
        end

        add resource, context
      end

      @parser = parser unless context.empty?
      parser
    end

    def parse(input, context = {})
      parser(context).parse input
    end

    def read(input)
      Zip.warn_invalid_date = false

      Zip::File.open input do |zip_file|
        zip_file.glob('**/*.xml').each do |entry|
          entry_input = entry.get_input_stream

          line_parser = Source.new(supported_resources: %i[Line Company])
          line_parser.parse entry_input

          line_id = line_parser.lines.first&.id
          company_id = line_parser.companies.first&.id

          parse entry_input, line_id: line_id, company_id: company_id
        end
      end

      flush
    end

    class Mapper
      attr_reader :element

      def initialize(element)
        @element = element
      end

      def self.mapper_class_for(name)
        const_get name
      rescue StandardError
        self
      end

      def self.for(name_or_element, element = nil)
        name, element = element ? [name_or_element, element] : [name_or_element.name, name_or_element]
        mapper_class_for(name).new(element)
      end

      def self.underscore(string)
        string.gsub(/([a-z]+)([A-Z0-9])/, '\1_\2').downcase
      end

      def underscore(string); self.class.underscore(string); end

      def self.capitalize_first(string)
        "#{string[0].upcase}#{string[1..]}"
      end

      def self.report_ignore_element(element)
        # if (@report_ignore_element ||= Set.new).add?(element)
        #   puts "Ignore #{element}"
        # end
      end

      def resource
        @resource ||= create_resource
      end

      def resource_class
        # FIXME: for xml-source
        @resource_class ||= Object.const_get("Neptune::#{self.class.capitalize_first(element.name)}")
      end

      def create_resource
        resource_class.new
      end

      def map_attributes
        element.attributes.each do |name, value|
          attribute = underscore(name)
          attribute_writer = "#{attribute}="

          resource.send(attribute_writer, value)
        end
      end

      def map_elements
        element.elements.each do |child|
          attribute = underscore(child.name)

          custom_map_method = "map_#{attribute}"
          if respond_to?(custom_map_method)
            send custom_map_method, child
            next
          end

          attribute_writer = "#{attribute}="
          unless resource.respond_to? attribute_writer
            self.class.report_ignore_element "#{element.name}/#{child.name}"
            next
          end

          value =
            # FIXME: for xml-source
            # if child.name.end_with?("Ref")
            #   map_reference child
            # elsif child.name == Mapper::KeyList.tag_name
            #   map_key_list child
            if !child.has_elements?
              child.get_text&.value&.strip
            else
              Mapper.for(child).map
            end

          resource.send(attribute_writer, value)
        end
      end

      def map
        map_attributes
        map_elements

        resource unless resource.empty?
      end
    end

    class Mapper
      class VehicleJourney < Mapper
        def map_vehicle_journey_at_stop(xml)
          resource.vehicle_journey_at_stops << Mapper.for('VehicleJourneyAtStop', xml).map
        end
      end
    end

    class Mapper
      class Timetable < Mapper
        def map_vehicle_journey_id(xml)
          resource.vehicle_journey_ids << xml.get_text&.value&.strip
        end

        def map_day_type(xml)
          resource.day_types << xml.get_text&.value&.strip
        end

        def map_calendar_day(xml)
          resource.calendar_days << xml.get_text&.value&.strip
        end

        def map_period(xml)
          resource.periods << Mapper.for('Period', xml).map
        end
      end
    end
  end
end
