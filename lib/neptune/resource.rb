# frozen_string_literal: true

module Neptune
  class Resource
    # FIXME: for xml-tag (shared ?)
    def initialize(attributes = {})
      attributes.each { |k, v| send "#{k}=", v }
    end

    def self.short_name
      # FIXME: for xml-tag
      @short_name ||= name.gsub('Neptune::', '').freeze
    end

    def self.name_of_class
      @name_of_class ||= short_name.gsub(/([a-z]+)([A-Z])/, '\1_\2').downcase
    end

    def name_of_class
      self.class.name_of_class
    end

    attr_accessor :raw_xml

    def tags
      @tags ||= {}
    end
    attr_writer :tags

    def tag(name)
      tags[name.to_sym]
    end

    def empty?
      # TODO: use Contents
      instance_variables.all? do |attribute|
        value = instance_variable_get(attribute)
        value.nil?
      end
    end
  end

  class Entity < Resource
    attr_accessor :object_id, :object_version, :creation_time
    alias id object_id
    alias version object_version
    alias created_at creation_time
  end

  class StopPoint < Entity
    attr_accessor :name, :longitude, :latitude
  end

  class Company < Entity
    attr_accessor :name
    # TODO: registration_number
  end

  class Line < Entity
    attr_accessor :name, :published_name, :number, :transport_mode_name, :company_id
    alias transport_mode transport_mode_name
    # TODO: registration_number
  end

  module LineResource
    attr_accessor :line_id
  end

  class VehicleJourney < Entity
    include LineResource

    attr_accessor :route_id, :journey_pattern_id, :published_journey_name

    def vehicle_journey_at_stops
      @vehicle_journey_at_stops ||= []
    end
    alias stops vehicle_journey_at_stops
  end

  class VehicleJourneyAtStop < Resource
    attr_accessor :stop_point_id, :vehicle_journey_id, :arrival_time, :departure_time
  end

  class Timetable < Entity
    attr_accessor :comment

    def periods
      @periods ||= []
    end

    def vehicle_journey_ids
      @vehicle_journey_ids ||= Set.new
    end

    def day_types
      @day_types ||= []
    end

    def calendar_days
      @calendar_days ||= ConvertedCollection.new { |s| Date.parse s }
    end
  end

  class Period < Resource
    attr_reader :begin, :end

    # Basic support. See GTFS-16
    def begin=(raw_value)
      @begin = raw_value.is_a?(String) ? Date.parse(raw_value) : raw_value
    end

    def end=(raw_value)
      @end = raw_value.is_a?(String) ? Date.parse(raw_value) : raw_value
    end

    alias start_of_period begin
    alias end_of_period end
    alias start_of_period= begin=
    alias end_of_period= end=

    def ==(other)
      other && self.begin == other.begin && self.end == other.end
    end

    def to_s
      "#{self.begin}..#{self.end}"
    end

    def inspect
      "#<Neptune::Period: #{self.begin}..#{self.end}>"
    end
  end

  # Basic support. See GTFS-16
  class ConvertedCollection < SimpleDelegator
    def initialize(&block)
      @array = []
      @converter = block

      super @array
    end

    attr_reader :converter

    def convert(raw_value)
      raw_value.is_a?(String) ? converter.call(raw_value) : raw_value
    end

    def push(raw_value)
      @array << convert(raw_value)
    end
    alias << push
  end
end
