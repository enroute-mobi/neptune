# frozen_string_literal: true

require 'saxerator'

# Ensure Saxerator stackes UTF-8 strings (required by REXML).
# See NEPTUNE-6
module Saxerator
  module Parser
    class Accumulator
      def characters(string)
        string.encode!('UTF-8') unless string.encoding.name == 'UTF-8'
        @stack[-1].add_node(string) unless string.strip.empty?
      end
    end
  end
end

require 'tempfile'
require 'ox'
require 'forwardable'
require 'zip'
require 'time'

require 'neptune/version'

module Neptune
  class Error < StandardError; end
  # Your code goes here...
end

require 'neptune/resource'
require 'neptune/tester'
require 'neptune/source'
