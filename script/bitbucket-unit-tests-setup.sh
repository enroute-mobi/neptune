#!/bin/sh -e

# Configure UTF-8 locale
apt-get -qq update && apt-get -qq install -y --no-install-recommends locales
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && locale-gen
locale

# Install expected bundler version
gem install "bundler:$BUNDLER_VERSION"

# Install dependencies
export DEV_PACKAGES="build-essential git curl"
export RUN_PACKAGES=""
# shellcheck disable=SC2086
apt-get -qq -y install --no-install-recommends $DEV_PACKAGES $RUN_PACKAGES

# Install bundler dependencies
bundle install --quiet --jobs 4 --deployment
