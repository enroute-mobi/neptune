# Neptune

## What's a Neptune file ?

Neptune is a reference format for data exchange, collective transport, defined at French level. It provides a description of network topology, schedules and access to information services.

A Neptune file is simply a unique or group of XML files, that must be compliant with the Neptune XML schema.

For French readers, you’ll find a lot of resources on the [Normes Données TC - Neptune](http://www.normes-donnees-tc.org/format-dechange/donnees-theoriques/neptune/) page.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'neptune'
```

And then execute:

```console
$ bundle install
```

Or install it yourself as:

```console
$ gem install neptune
```

## Usage

This gem read Neptune (aka Trident) files (French Technical Standard for exchanging Public Transport schedules and related data).

```ruby
source = Neptune::Source.new
source.parse input

source.companies do |companies|

end

source.lines do |lines|

end

source.vehicle_journeys do |vehicle_journey|
  vehicle_journey.stops.each do |stop|

  end
end

source.stop_points.each do |stop_point|

end
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on BitBucket and GitHub at https://bitbucket.org/enroute-mobi/neptune/ or https://github.com/enroute-mobi/neptune.

## License

The gem is available as open source under the terms of the [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).
